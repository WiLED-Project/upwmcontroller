# uPWMController

A MicroPython library for controlling PWM outputs using an asyncio loop.


## Installation

Installing is easy - just copy the `upwmcontroller.py` file to your MicroPython board. It can live either in the root, or in a `/lib` folder. 

The file is only a few kilobytes, but for more efficiency it can be frozen into a MicroPython binary image - just place the file inside the `ports/<board>/modules` folder when building MicroPython from source, then flash to the board as usual. 


## Usage

Usage is fairly simple, although some knowledge and experience with MicroPython's `uasyncio` library will be helpful. 

Create an instance of `uPWMController` like so:

```python
led = uPWMController(machine.PWM(machine.Pin(17), duty=0))
```

Then when setting up the event loop, add the `run()` coroutine as a task:

```python
# Get a reference to the event loop
loop = uasyncio.get_event_loop()
# Create an instance of the background coroutine
led_coro = uasyncio.coroutine(led.run())
# Schedule the coroutine to run ASAP
loop.create_task(led_coro)
```

Then start the loop as required for the rest of your code, probably with `run_forever()`. 

When the event loop is running, the `set_duty()` function is used to start fading the PWM output, for example:

```python
led.set_duty(1023, fadetime=1000)
```

The maximum duty value will depend on the resolution of the PWM module for your particular board, on an ESP32 the maximum is 1023 (i.e. 10 bits). 

See the example in [`examples/main.py`](./example/main.py) for a demo of controlling two LEDs and running an event loop for a short period of time. 


## License

uPWMController, A MicroPython library for controlling PWM outputs

Copyright (C) 2019, Sean Lanigan

uPWMController is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

uPWMController is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with uPWMController.  If not, see <https://www.gnu.org/licenses/>.

See [LICENSE](./LICENSE) for the full text. 
